<?php

session_start();
include '../bbdd/DataBaseMan.php';
include '../bbdd/ContactDAO.php';

//only executes this if there's a session
if (isset($_SESSION["idUser"])) {
    $data = explode(',', $_POST['delContacts']);
    $contactDao = new ContactDao();
    foreach ($data as $d) {
        if ($contactDao->delete($d)) {
            echo "<script>console.log( " . $d . " failed);</script>";
        }
    }
    $_POST['delContacts'] = null;
} else {
     $_POST['delContacts'] = null;
    header("Location: ../agenda.php");
}
?>