<?php

session_start();
include '../bbdd/DataBaseMan.php';
include '../bbdd/ContactDAO.php';

//only executes this if there's a session
if (isset($_SESSION["idUser"])) {
    $contactDao = new ContactDao();

    if (isset($_POST["new"])) {
        $data = explode(',', $_POST['new']);
        $contactDao->insert($_SESSION["idUser"], $data[0], $data[1], $data[2], $data[3]);
    } else if (isset($_POST["edit"])) {
        $data = explode(',', $_POST['edit']);
        $contactDao->updateAll($_SESSION["idUser"], $data[0], $data[1], $data[2], $data[3],$data[4]);
    }
    
    $_POST['new'] = null;
    $_POST['edit'] = null;
} else {
    $_POST['new'] = null;
    $_POST['edit'] = null;
    header("Location: ../agenda.php");
}
?>