<!-- Header used in every page but home.php -->
<?php
if(!empty($_GET) && isset($_GET['logout'])){
    session_destroy();
    header("Location: ./home.php");
}
?>
<link rel="stylesheet" type="text/css" href="./css/header.css" />
<header>
    <a href="agenda.php"><img src="./img/Logo.png" id="logo"/>
    <h1>Agenda Online</h1></a>
    <div id="user"> 
        <img src="./img/userCut.png" id="userIcon"/>
        <div class="dropdown">
            <button class="username"><?php echo $_SESSION['userName'] ?></button>
            <div class="dropdown-content">
                <a href="<?php echo $_SERVER['PHP_SELF']; ?>?logout=true">Logout</a>
            </div>
        </div>
    </div>
</header>



