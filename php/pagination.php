<link rel="stylesheet" type="text/css" href="./css/pagination.css" />
<div id="navigation">
    <ul class="pagination">
        <?php
        if (empty($_GET)) {
            $page = 1;
        } else {
            $page = $_GET['page'];
        }

        $back = "agenda.php?page=" . ($page - 1);
        $forward = "agenda.php?page=" . ($page + 1);
        $totalContactsArray = $contactDao->countContacts($_SESSION["idUser"]);

        if (isset($_GET['search'])) {
            $searchString = "&search=" . $_GET['search'];
            $back .= $searchString;
            $forward .= $searchString;
            $totalContactsArray = $contactDao->countSContacts($_GET['search'], $_SESSION["idUser"]);
        }

        $totalContacts = ceil($totalContactsArray[0]["count(*)"] / $contactsPerPage);
        if ($page != 1) {
            ?><li><a href="<?php echo $back; ?>">«</a></li><?php
        }

        for ($i = ($page - $paginationFromActive <= 0 ? 1 : $page - $paginationFromActive ); $i <= (($page + $paginationFromActive) > $totalContacts ? $totalContacts : $page + $paginationFromActive ); $i++) {
            $link = "agenda.php?page=" . $i;
            if (isset($_GET['search'])) {
                $link .= "&search=" . $_GET['search'];
            }
            if ($i == $page) {
                ?>
                <li><a class="active" href="<?php echo $link; ?>"><?php echo $i; ?></a></li>
                    <?php
                } else {
                    ?>
                <li><a href="<?php echo $link; ?>"><?php echo $i; ?></a></li><?php
            }
        }
        if ($page != $totalContacts && $totalContacts != 0) {
            ?><li><a href="<?php echo $forward ?>">»</a></li><?php
            }
            ?>
    </ul><br>    
</div> 
