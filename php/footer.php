<!-- Footer used in every page -->
<link rel="stylesheet" type="text/css" href="./css/footer.css" />
<footer>
    <div id = "footercontacts">
        <img src="./img/icons/phone.png" class="contactsImage"/> +34 999 999 999
        <img src="./img/icons/email.png" class="contactsImage"/>  agenda@iam.cat
        <img src="./img/icons/address.png" class="contactsImage"/> Crta Esplugues 56 08860 Barcelona
    </div>

    <div id="copyright">Copyright © 2016 DAW Sara Henriques </div>

</footer>