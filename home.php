<?php
session_start();

if (isset($_SESSION["idUser"])) {
    header("Location: ./agenda.php");
}
?>
<html>
    <head>
        <title>Agenda</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./css/home.css">
    </head>
    <body>       
        <div id="header">
            <img class="slide" src="./img/header_Home_cut.jpg" alt="Agenda Online">
            <a href="./agenda.php">
                <img class="logo" src="./img/Logo.png" alt="Sar&Nic" width="100" height="100"/>
            </a>
            <div class="login">            		
                <h1 class="home">Agenda Online</h1>
                <form class="login" action='<?php echo $_SERVER['PHP_SELF'] ?>' method="POST">
                    <div class="usrpass">
                        <input type="text" name="usuario" placeholder="usuario"/><br>
                        <input type="password" name="password" placeholder="contraseña"/><br>
                        <p id='warning'></p>
                    </div>
                    <input type="submit" value="Login"/>
                </form>
            </div>
            <div class="enter"></div>
        </div>
        <div class="enter"></div>
        <div id="info">
            <h2 class="home">Descubre lo que Agenda Online puede hacer por ti</h2>
            <div class="two-colums">
                <img src="./img/home_desc_img.PNG" alt="">
                <ul>
                    <li>Gestiona facilmente tus contactos en cualquier lado.</li>
                    <li>Administra y organiza tus contactos cómodamente.</li>
                    <li>Búsqueda contactos por nombre y/o apellidos.</li>
                    <li></li>
                    <li>¿Te has quedado sin batería en el móvil? <br> Agenda Online es tu mejor opción!</li>        		
                </ul>
                <div class="enter"></div>
            </div>        
        </div>
        <?php
        if ($_POST) {
            include './bbdd/DataBaseMan.php';
            include './bbdd/UserDAO.php';
            $userdao = new UserDAO();

            if ($userdao->login($_POST['usuario'], $_POST['password'])) {
                header("Location: ./agenda.php");
            } else {
                ?>
                <script>
                    document.getElementById("warning").innerHTML = "Login incorrecto";
                </script>

                <?php
            }
        }

        include("./php/footer.php");
        ?>
    </body>
</html>
