#AGENDA

*requesitos*  
tener git, mysql y apache instalado  

*apache*  
Guardar este fichero de configuración en /etc/apache2/sites-available y activarla ( a2ensite )  
```

<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/html/	

	<Directory /var/www/html/>
       		Options Indexes FollowSymLinks
        	AllowOverride None
        	Require all granted
	</Directory>

	
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

```
  
*git*  
```cd /var/www/html/```  
```git clone https://anotherH@bitbucket.org/anotherH/agenda_proyecto_transversal.git``` 

*base de datos*  
Ejecutar todas las queries del fichero scripts/dbScript_creacion.sql.  
Si se desea datos de prueba ejecutar también las queries del fichero scripts/dbScript_datos_prueba.sql.  

*usuarios de prueba*  
nombre: usuario1
contraseña: prova

nombre: usuario2
contraseña: patata