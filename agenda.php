<?php
session_start();
include './bbdd/DataBaseMan.php';
include './bbdd/ContactDAO.php';

if (!isset($_SESSION["idUser"])) {
    header("Location: ./home.php");
}

if (empty($_GET)) {
    $page = 1;
} else {
    $page = $_GET['page'];
}

$contactDao = new ContactDao();
$contactsPerPage = 10;
$paginationFromActive = 3;
?>
<html>
    <head>
        <title>Agenda</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./css/agenda.css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    </head>
    <body>
        <?php include("./php/header.php"); ?>
        <div id="content">
            <div id="secondHeader">
                <h2>Mis Contactos</h2>
                <input type="text"  placeholder="Buscar..." id="search"/>
                <input type="image" class="icons" src="./img/icons/search.png" onclick="search()"/>
                <input type="image" class="icons" src="./img/icons/plus.png" onclick="location.href = './addcontact.php'"/>
                <input type="image" class="icons" src="./img/icons/eliminate.png" onclick='showModalAgenda()'/>
            </div>
            <div id="contacts">
                <p id='warning'> </p>
                <?php
                include("./php/pagination.php");
                if (!isset($_GET['search'])) {
                    $pagecontacts = $contactDao->getCurrentPage($_SESSION["idUser"], $page);
                } else {
                    $pagecontacts = $contactDao->searchContact($_GET['search'], $_SESSION["idUser"], $page);
                }

                if (count($pagecontacts) === 0) {
                    ?>
                    <script>
                        document.getElementById("warning").innerHTML = "No se encontraron contactos";
                        document.getElementById("warning").style.paddingTop = "100px";
                    </script>
                    <?php
                }
                foreach ($pagecontacts as $contact) {
                    ?>
                    <div class="contact">
                        <input type="checkbox" id='<?php echo $contact['ID_CONTACT']; ?>'/>
                        <a href="./contact.php?id=<?php echo $contact['ID_CONTACT']; ?>">
                            <h3><?php echo $contact["NAME"] . " " . $contact["SURNAME"]; ?> 
                                <span><?php echo $contact["TELF"]; ?></span>
                            </h3>
                        </a>
                    </div>
                    <?php
                }
                ?>
            </div> 

            <!-- The Modal -->
            <div id="myModal" class="modal">

                <!-- Modal content -->
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="close">×</span>
                        <h2>Borrar Contactos</h2>
                    </div>
                    <div class="modal-body">
                        <p id='modal-message'></p>
                    </div>
                    <div class="modal-footer">
                        <button class="return" onclick='modal.style.display = "none";'>volver</button>
                        <button type="button" class="accept" onclick='onClickFromAgendaDelete()'> borrar</button>
                    </div>
                </div>

            </div>

            <?php include("./php/pagination.php"); ?>
        </div>
        <?php include("./php/footer.php"); ?>

        <script>
            /**
             * This is the funcion used to search the contacts
             * @returns {undefined}
             */
            function search() {
                var searchBar = document.getElementById('search');
                if (searchBar.value) {
                    location.href = './agenda.php?page=1&search=' + searchBar.value;
                } else {
                    location.href = './agenda.php?page=1';
                }
            }

            document.getElementById("search")
                    .addEventListener("keyup", function (event) {
                        var key = event.which || event.keyCode;
                        if (key == 13) {
                            search();
                        }
                    });
        </script>

        <script type='text/javascript' src="./js/modal.js"></script>
    </body>
</html>
