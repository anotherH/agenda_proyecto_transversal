<?php

include './DataBaseMan.php';

class ContactDao {

    /**
     * Selects All contacts
     * currently not used
     * @param type $idUser
     * @return type
     */
    public function selectAll($idUser) {
        $db = new DatabaseMan();
        $sql = "select * from `CONTACT` where `ID_USER_FK`=" . $idUser;
        return $db->select($sql);
    }

    /**
     * Gets all contacts from current page
     * @param type $idUser
     * @param type $pagination
     * @return type
     */
    public function getCurrentPage($idUser, $pagination) {
        $db = new DatabaseMan();
        $sql = "select `ID_CONTACT`, `NAME`, `SURNAME`, `TELF` from `CONTACT` where `ID_USER_FK`=" . $idUser .
                " order by `NAME`,`SURNAME` " .
                " LIMIT " . ($pagination === 1 ? 0 : 10 * $pagination - 10) . "," . (10 * $pagination);
        return $db->select($sql);
    }

    /**
     * pagination.php
     * @param type $idUser
     * @return type
     */
    public function countContacts($idUser) {
        $db = new DatabaseMan();
        $sql = "select count(*) from `CONTACT` where `ID_USER_FK`= " . $idUser;
        return $db->select($sql);
    }


    /**
     * Inserts new user
     * @param type $idUser
     * @param type $contactFirstName
     * @param type $contactSecondName
     * @param type $telef
     * @param type $date
     * @return type
     */
    public function insert($idUser, $contactFirstName, $contactSecondName, $telef, $date) {
        $db = new DatabaseMan();
        $sDate = $date ? "'".$date."'" : "null";
        $sql = "INSERT INTO CONTACT (ID_USER_FK, NAME, SURNAME, TELF, BIRTHDATE) VALUES (" . $idUser . ", '" . $contactFirstName . "', '" . $contactSecondName . "', '" . $telef . "', " . $sDate . ")";
        return $db->query($sql);
    }

    /**
     * Deletes user
     * @param type $idContact
     * @return type
     */
    public function delete($idContact) {
        $db = new DatabaseMan();
        $sql = "DELETE FROM CONTACT WHERE `ID_CONTACT`='" . $idContact . "'";
        return $db->query($sql);
    }

    /**
     * The user only searches for one name at the time
     * for firstname or surname
     * @param type $name
     * @param type $pagination
     * @return type
     */
    public function searchContact($name, $idUser, $pagination) {
        $db = new DatabaseMan();
        $sql = "select `ID_CONTACT`, `NAME`, `SURNAME`, `TELF` from `CONTACT` WHERE "
                . "(`NAME`= '" . $name . "' or `SURNAME` = '" . $name . "' ) and `ID_USER_FK`=" . $idUser .
                " order by `NAME`,`SURNAME` " .
                " LIMIT " . ($pagination === 1 ? 0 : 10 * $pagination - 10) . "," . (10 * $pagination);
        return $db->select($sql);
    }
    
        /**
     * pagination.php
     * @param type $idUser
     * @return type
     */
    public function countSContacts($name,$idUser) {
        $db = new DatabaseMan();
        $sql = "select count(*) from `CONTACT` WHERE "
                . "(`NAME`= '" . $name . "' or `SURNAME` = '" . $name . "' ) and `ID_USER_FK`=" . $idUser;
        return $db->select($sql);
    }
    
    public function getContact($idContact, $idUser){
        $db = new DatabaseMan(); 
        $sql = "select * from `CONTACT` WHERE "
                . "`ID_CONTACT` = ". $idContact." and `ID_USER_FK`=" . $idUser;
        return $db->select($sql);
    }

    public function updateAll($idUser,$contactFirstName, $contactSecondName, $telef, $date,$idContact){
        $db = new DatabaseMan();
        $sDate = $date ? "'".$date."'" : "null";
        $sql = "UPDATE `CONTACT` SET `NAME` = '".$contactFirstName."' , `SURNAME` ='".$contactSecondName."', `TELF` ='".$telef."', `BIRTHDATE` =".$sDate 
                . " WHERE `ID_USER_FK`=" . $idUser . " AND `ID_CONTACT`= ". $idContact;
        echo $sql;
        return $db->query($sql);
    }
}
