<?php

/*
 * This class manages every connection or query to the database
 * It's meant to simplify the interaction.
 * 
 */

class DatabaseMan {

    // The database connection
    protected static $connection;

    /**
     * Creates the connection.
     * If it's not able to connect is throws an exception
     * @return boolean
     */
    public function connect() {
        //this should be in a config.ini
        $dbHost = 'localhost';
        //LOCAL:
//        $dbUser = 'agenda';
//        $dbName = 'Agenda';
        //PRO
        $dbName = 'a14sarhenmen_Agenda';
        $dbUser = 'a14sarhenmen_age';
        $dbPass = 'agenda123';

        // Try and connect to the database, if a connection has not been established yet
        if (!isset(self::$connection)) {
            self::$connection = new mysqli($dbHost, $dbUser, $dbPass, $dbName);
        }

        // If connection was not successful, handle the error
        if (self::$connection === false) {
            throw new Exception('Cant connect to database');
        }

        return self::$connection;
    }

    /**
     * Makes a query to the database and returns the result
     * @param type $query
     * @return type
     */
    public function query($query) {
        // Connect to the database
        try {
            $connection = $this->connect();
        } catch (Exception $e) {
            //this exception should be logged 
            //echo 'Excepción capturada: ', $e->getMessage(), "\n";
            return false;
        }
        // Query the database
        $result = $connection->query($query);
        return $result;
    }

    /**
     * Makes a select and returns rows
     * if query fails, it returns false
     * if query is successful, returns true
     * @param type $query
     * @return boolean
     */
    public function select($query) {
        $rows = array();

        $result = $this->query($query);
        // If query failed, return `false`
        if ($result === false) {
            return false;
        }

        // If query was successful, retrieve all the rows into an array
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

}
