var monthString = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
var date = new Date();
var month = date.getMonth();
var year = date.getFullYear();
var calendar = document.getElementById('calendar');

function generateCalendarForMonth(month, year) {
    var ul = document.getElementById('days');
    while (ul.hasChildNodes()) {
        ul.removeChild(ul.firstChild);
    }
    //title with the correct month and year
    document.getElementById('monthYear').innerHTML = monthString[month] + " " + year;

    var daysMax = getMaxDay(month, year);

    for (i = 1; i <= daysMax; i++) {
        if (i === 1) {
            getFirstDay(month, year);
        }
        var day = document.createElement("li");
        if (i === date.getDate() && month === date.getMonth() && year === date.getFullYear()) {
            day.className = "active";
        }
        day.innerHTML = i;
        ul.appendChild(day);
    }
}


function getMaxDay(month, year) {
    if (month === 0 || month === 2 || month === 4 || month === 6 || month === 7 || month === 9 || month === 11) {
        return 31;
    } else if (month === 1) {
        if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
            return 29;
        else
            return  28;
    } else {
        return 30;
    }
}

function getFirstDay(month, year) {
    var ul = document.getElementById('days');
    var date = new Date(year, month, i);
    var dayOfWeek = date.getDay();
    for (j = 0; j < dayOfWeek; j++) {
        var day = document.createElement("li");
        ul.appendChild(day);
    }
}

function backMonth() {
    if (month === 0) {
        year--;
        month = 11;
    } else {
        month--;
    }
    generateCalendarForMonth(month, year);
}

function fowardMonth() {
    if (!(year === date.getFullYear() && month === date.getMonth())) {
        if (month === 11) {
            year++;
            month = 0;
        } else {
            month++;
        }
        generateCalendarForMonth(month, year);
    }
}

function backYear() {
    year--;
    generateCalendarForMonth(month, year);
}

function fowardYear() {
    if (year !== date.getFullYear()) {
        year++;
        generateCalendarForMonth(month, year);
    }
}

function selectDate(day) {
    document.getElementById('birthdate').setAttribute("value", year + "/" + (month + 1) + "/" + day);
    calendar.style.display = "none";
}

window.onclick = function (event) {
    if (event.target.tagName.toLowerCase() == 'li' && event.target.parentNode === document.getElementById('days')) {
        var target = event.target || event.srcElement;
        selectDate(target.innerHTML);
    }
}
