//including other js
var js = document.createElement("script");
js.type = "text/javascript";
js.src = "./js/general.js";
document.body.appendChild(js);


// Get the modal
var modal = document.getElementById('myModal');
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
// When the user clicks on <span> (x), close the modal
var ids = null;

/*ON CLICKS*/
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        if (ids !== null) {
            window.location.reload();
        }
        modal.style.display = "none";
    }
}

/*FUNCTIONS*/

/**
 * Returns a array with the ids to delete
 * Function used in Agenda
 * @returns {Array|getContactsToDelete.ids}
 */
function getContactsToDelete() {
    var inputs = document.getElementsByTagName("input");
    ids = new Array();
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].type == "checkbox" && inputs[i].checked == true) {
            ids.push(inputs[i].id);
        }
    }
    return ids;
}

/**
 * This function is used in Agenda
 * If theres not contacts selected it shows a message for the used to select some.
 * @returns {undefined}
 */
function showModalAgenda() {
    showModal();

    ids = getContactsToDelete();
    if (!ids.length) {
        document.getElementById("modal-message").innerHTML = "No hay contactos seleccionados. Por favor, seleccionar los contactos a borrar.";
        document.getElementsByClassName("accept")[0].style.display = "none";
    } else {
        document.getElementById("modal-message").innerHTML = "Los contactos selecionados seran borrados.";
        document.getElementsByClassName("accept")[0].style.display = "inline";
    }
}

/**
 * Makes the modal visible
 * @returns {undefined}
 */
function showModal() {
    modal.style.display = "block";
}

/**
 * onclick of the button confirm to delete from the Agenda
 * @returns {undefined}
 */
function onClickFromAgendaDelete() {
    deleteChars(getContactsToDelete());
}

/**
 * onclick of the button confirm to delete from the Contact
 * @returns {undefined}
 */
function onClickFromContactDelete() {
    deleteChars(getURLParams('id'));
}

/**
 * This function deletes the characters
 * @param {type} ids can be a array of ints or a single int
 * @returns {undefined}
 */
function deleteChars(ids) {
    $.post("./php/delcontact.php", {delContacts: ids.toString()})
            .done(function () {
                document.getElementById("modal-message").innerHTML = "Contactos Borrados";
                document.getElementsByClassName("accept")[0].style.display = "none";
                document.getElementsByClassName("return")[0].onclick = function () {
                    window.location.reload();
                }
            });
}









