<?php
session_start();
include './bbdd/DataBaseMan.php';
include './bbdd/ContactDAO.php';

if (!isset($_SESSION["idUser"])) {
    header("Location: ./home.php");
}

if (!isset($_GET['id'])) {
    header("Location: ./agenda.php");
} else {
    $contactDao = new ContactDao();
    $contact = $contactDao->getContact($_GET['id'], $_SESSION['idUser'])[0];
    if(!$contact){
        header("Location: ./agenda.php");
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Agenda</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./css/contact.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
</head>
<body>
    <?php include("./php/header.php"); ?>
    <div id="content">
        <div id="secondHeader">
            <h2><?php echo $contact['NAME'] . " " . $contact['SURNAME']; ?></h2>
            <input type="image" class="icons" src="./img/icons/eliminate.png" onclick="showModal()"/>

            <input type="image" class="icons" src="./img/icons/edit.png" onclick="location.href = './addcontact.php?id=<?php echo $contact['ID_CONTACT']; ?>'"/>
        </div>
        <div id="contactInfo">
            <h3><img src="./img/icons/telefono.png" alt=""/><?php echo $contact['TELF']; ?></h3>
            <h3><img src="./img/icons/fec_nac.png" alt=""/><?php echo $contact['BIRTHDATE']; ?></h3>   

        </div> 
        <div id="contactButton">
            <button class="return2" Type="button" onClick="history.go(-1);return true;"> Volver </button>
        </div> 

        <!-- The Modal -->
        <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close">×</span>
                    <h2>Borrar Contactos</h2>
                </div>
                <div class="modal-body">
                    <p id='modal-message'>Los contactos selecionados seran borrados.</p>
                </div>
                <div class="modal-footer">
                    <button class="return" onclick='modal.style.display = "none";'>volver</button>
                    <button type="button" class="accept" onclick='onClickFromContactDelete()'> borrar</button>
                </div>
            </div>

        </div>
    </div>
    <?php include("./php/footer.php"); ?>

    <script type='text/javascript' src="./js/modal.js"></script>
</body>
</html>
