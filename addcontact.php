<?php
session_start();
include './bbdd/DataBaseMan.php';
include './bbdd/ContactDAO.php';

if (!isset($_SESSION["idUser"])) {
    header("Location: ./home.php");
}
$contact = false;
if (!isset($_GET['id'])) {
    //NEW CONTACT
} else {
    $contactDao = new ContactDao();
    $contact = $contactDao->getContact($_GET['id'], $_SESSION['idUser'])[0];
}
?>
<html>
    <head>
        <title>Agenda</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./css/addcontact.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    </head>
    <body>
        <?php include("./php/header.php"); ?>
        <div id="content">
            <div id="secondHeader">
                <?php if (!$contact) { ?>
                    <h2>Añadir Nuevo Contacto</h2>
                <?php } else {
                    ?>
                    <h2>Editar Contacto</h2>
                <?php } ?>

            </div>
            <div id="contactInfo">
                <form id='form'>
                    <img src="./img/icons/nombre.png" alt="">
                    <input type="text" id="name" pattern="^[a-zA-Z]+( [a-zA-Z]+)*$"
                    <?php if (!$contact) { ?>
                               placeholder = "nombre"<?php
                           } else {
                               echo "value='" . $contact['NAME'] . "'";
                           }
                           ?> required>
                    <input type="text" id="surname" pattern="^[a-zA-Z]+( [a-zA-Z]+)*$"
                    <?php if (!$contact) { ?>
                               placeholder = "apellidos"<?php
                           } else {
                               echo "value='" . $contact["SURNAME"] . "'";
                           }
                           ?>>


                    <div class="enter"></div>

                    <img src="./img/icons/telefono.png" alt="">
                    <input type="tel" id="telf" pattern="[+0-9]{0,13}"
                    <?php if (!$contact) { ?>
                               placeholder = "telefono"<?php
                           } else {
                               echo "value='" . $contact["TELF"] . "'";
                           }
                           ?> required>
                    <div class="enter"></div>

                    <img src="./img/icons/fec_nac.png" alt="">
                    <!-- http://html5pattern.com/Dates "-->
                    <input  type="date" id="birthdate" onClick="document.getElementById('calendar').style.display = 'block'; generateCalendarForMonth(month, year);"
                            pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])"
                            <?php if (!$contact) { ?>
                                placeholder = "AAAA-MM-DD"<?php
                            } else {
                                echo "value='" . $contact["BIRTHDATE"] . "'";
                            }
                            ?>>
                    <div class="enter"></div>
                    <div id="calendar">
                        <div class="month">      
                            <ul>
                                <li class="prev" onclick="backYear()">❮</li>
                                <li class="prev" onclick="backMonth()"><</li>
                                <li class="next" onclick="fowardYear()">❯</li>
                                <li class="next" onclick="fowardMonth()">></li>
                                <li style="text-align:center" id="monthYear"></li>
                            </ul>
                        </div>
                        <ul class="weekdays">
                            <li>Dom</li> 
                            <li>Lun</li>
                            <li>Mar</li>
                            <li>Mié</li>
                            <li>Jue</li>
                            <li>Vie</li>
                            <li>Sáb</li>
                        </ul>
                        <ul id="days"></ul> 
                    </div>
                </form>
            </div> 
            <div id="contactButton">
                <button class="return" Type="button" onClick="history.go(-1);return true;"> Volver </button>
                <button class="accept" Type="button" name="submit" onclick = 'onClickBoth()'> Guardar </button>
            </div> 
        </div>
        <div class="enter"></div>
        <?php include("./php/footer.php"); ?>
        <script type='text/javascript' src="./js/general.js"></script>
        <script>

                    function onClickBoth() {
                        var name = document.getElementById('name').value;
                        var surname = document.getElementById('surname').value;
                        var telf = document.getElementById('telf').value;
                        var birthdate = document.getElementById('birthdate').value;
                        var stringToPost = name + "," + surname + "," + telf + "," + birthdate;
                        //only valid if name and telf not null
                        var isValid = name.length !== 0 || telf.length !== 0;
                        if (getURLParams('id') === null && isValid) {
                            onClickNewContact(stringToPost);
                        } else if (getURLParams('id') !== null && isValid) {
                            onClickEditContact(stringToPost + "," + getURLParams('id'));
                        } else {
                            alert("El nombre y telefono son obligatorios");
                        }
                    }

                    function onClickNewContact(stringToPost) {
                        $.post("./php/AddOrEditContact.php", {new : stringToPost})
                                .done(function () {
                                    window.location = "./agenda.php";
                                });
                    }

                    function onClickEditContact(stringToPost) {
                        $.post("./php/AddOrEditContact.php", {edit: stringToPost})
                                .done(function () {
                                    window.location = "./agenda.php";
                                });
                    }
        </script>

        <script type='text/javascript' src='./js/calendario.js'></script>
    </body>
</html>
